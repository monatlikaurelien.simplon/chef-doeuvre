DROP TABLE IF EXISTS destination;
DROP TABLE IF EXISTS img;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS contact;
USE blogvoyage;


 CREATE TABLE `destination`
 (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  label VARCHAR(32)
 );

CREATE TABLE `post` 
(  
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(128),
    date DATE NOT NULL,
    content TEXT NOT NULL,
    img_label TEXT,
    destination_id INT,
    CONSTRAINT FK_post_destination
    FOREIGN KEY (destination_id)
    REFERENCES `destination`(id)
    ON DELETE CASCADE 
);

CREATE TABLE img
(
  id  INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  label VARCHAR (128),
  upload VARCHAR (128),
  post_id INT,
  CONSTRAINT FK_img_post
  FOREIGN KEY (post_id)
  REFERENCES post(id)
  ON DELETE CASCADE
);

CREATE TABLE contact
(
   id  INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR (128),
  firstName VARCHAR (128),
  email VARCHAR(128),
  `message` TEXT NOT NULL
);

INSERT INTO destination (label) VALUES
("Tokyo"),
("Osaka"),
("Kyoto"),
("Miyajima"),
("Hiroshima");

INSERT INTO post (destination_id, title, `date`, content, img_label) VALUES 
(1,
"Mon arrivé a Tokyo", 
"2018-05-08",
"Aprés plus de 10h de vol je suis enfin arrivé à Tokyo. Quelle belle ville.",
"img1"),

(3,
"Kyoto la ville de la Jungle",
 "2018-05-09",
 "Cette ville est positionée au beau milieux d'une immense jungle..",
"img1"),

(2,
 "Osaka ancienne capitale japonaise ", 
 "2018-05-10",
 "Au début du XXe siècle, Osaka était devenu un important centre industriel..",
"img1"),

(5,
 "Hiroshima mère de toutes les bombes", 
 "2018-05-10",
 "Hiroshima une ville sourd, le silence absolue..",
"img1"),

(4,
 "Miyajima l'ile au dain", 
 "2018-05-10",
 "Cette ville est peuplé de dain par milliés..",
"img1");


 INSERT INTO img (post_id, label, upload) VALUES
 (1,
 "rice-terraces", 
 "https://cdn.pixabay.com/photo/2017/06/10/05/26/rice-terraces-2389023_960_720.jpg"),

 (3,
 "mountain", 
 "https://cdn.pixabay.com/photo/2014/10/07/13/48/mountain-477832_960_720.jpg"),

 (2,
 "bridge", 
 "https://cdn.pixabay.com/photo/2012/08/06/00/53/bridge-53769_960_720.jpg"),

(5,
 "fushimi", 
 "https://cdn.pixabay.com/photo/2016/12/06/17/11/fushimi-inari-shrine-1886975_960_720.jpg"),

(4,
 "japan", 
 "https://cdn.pixabay.com/photo/2019/10/14/14/42/japan-4549082_960_720.jpg");

 INSERT INTO contact (`name`, firstName, email, `message`) VALUES
 ("Nom", 
 "Prénom", 
 "exemple@exemple.com", 
 "message");