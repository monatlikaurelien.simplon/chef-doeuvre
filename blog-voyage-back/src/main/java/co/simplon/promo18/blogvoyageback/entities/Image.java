package co.simplon.promo18.blogvoyageback.entities;

public class Image {
  private Integer id;
  private String label;
  private String upload;
  public Image() {}
  public Image(String label, String fileName) {
    this.label = label;
    this.upload = fileName;
  }
  public Image(Integer id, String label, String fileName) {
    this.id = id;
    this.label = label;
    this.upload = fileName;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }
  public String getUpload() {
    return upload;
  }
  public void setUpload(String fileName) {
    this.upload = fileName;
  }
}
