package co.simplon.promo18.blogvoyageback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.blogvoyageback.entities.Destination;
@Repository
public class DestinationRepository {
  @Autowired
  private DataSource dataSource;

  public List<Destination> findAll() {
    List<Destination> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM destination");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToDestination(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public Destination findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM destination WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();


      if (rs.next()) {
        return sqlToDestination(rs);

      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  public void save(Destination destination) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO destination (label) VALUES (?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, destination.getLabel());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        destination.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM destination WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Destination destination) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE destination SET label = ?, WHERE id = ?");

      stmt.setString(1, destination.getLabel());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

  }

  private Destination sqlToDestination(ResultSet rs) throws SQLException {
    return new Destination(rs.getInt("id"), rs.getString("label"));
  }
}
