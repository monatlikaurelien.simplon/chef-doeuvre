package co.simplon.promo18.blogvoyageback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogvoyagebackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogvoyagebackApplication.class, args);
	}

}
