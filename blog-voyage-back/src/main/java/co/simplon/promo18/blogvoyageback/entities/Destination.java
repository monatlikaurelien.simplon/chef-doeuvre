package co.simplon.promo18.blogvoyageback.entities;

public class Destination {
  private Integer id;
    private String label;
    public Destination() {}
    public Destination(String label) {
      this.label = label;
    }
    public Destination(Integer id, String label) {
      this.id = id;
      this.label = label;
    }
    public Integer getId() {
      return id;
    }
    public void setId(Integer id) {
      this.id = id;
    }
    public String getLabel() {
      return label;
    }
    public void setLabel(String label) {
      this.label = label;
    }
}
