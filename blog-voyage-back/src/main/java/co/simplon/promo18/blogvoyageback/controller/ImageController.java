package co.simplon.promo18.blogvoyageback.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.blogvoyageback.entities.Image;
import co.simplon.promo18.blogvoyageback.repository.ImageRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/img")
@RestController
public class ImageController {
  @Autowired
  private ImageRepository imgRepo;

  @GetMapping("/{id}")
  public List<Image> getAllByPost(@PathVariable int id) {
      return imgRepo.findAllByPost(id);
  }
  @GetMapping
  public List<Image> getAll() {
      return imgRepo.findAll();
  }
  
  

}
