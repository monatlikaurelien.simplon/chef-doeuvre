package co.simplon.promo18.blogvoyageback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.blogvoyageback.entities.Image;
@Repository
public class ImageRepository {
  
  @Autowired
  private DataSource dataSource;

  public List<Image> findAllByPost(int id) {
    List<Image> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM img WHERE post_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToImage(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }
  public List<Image> findAll() {
    List<Image> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM img");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToImage(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public Image findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM img WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();


      if (rs.next()) {
        return sqlToImage(rs);

      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  public void save(Image image) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO img (label, upload) VALUES (?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, image.getLabel());
      stmt.setString(1, image.getUpload());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        image.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM img WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Image image) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE img SET label = ?, upload = ?, WHERE id = ?");

      stmt.setString(1, image.getLabel());
      stmt.setString(1, image.getUpload());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

  }

  private Image sqlToImage(ResultSet rs) throws SQLException {
    return new Image(rs.getInt("id"), rs.getString("label"), rs.getString("upload"));
  }
}
