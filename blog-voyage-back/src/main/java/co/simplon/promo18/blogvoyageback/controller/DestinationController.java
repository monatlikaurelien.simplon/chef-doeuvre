package co.simplon.promo18.blogvoyageback.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.blogvoyageback.entities.Destination;
import co.simplon.promo18.blogvoyageback.repository.DestinationRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/destination")
@RestController
public class DestinationController {
  @Autowired
  private DestinationRepository dstRepo;

  @GetMapping
  public List<Destination> getAll() {
      return dstRepo.findAll();
  }
  @GetMapping("/{id}")
  public Destination getById(@PathVariable int id) {
      return dstRepo.findById(id);
  }

}
