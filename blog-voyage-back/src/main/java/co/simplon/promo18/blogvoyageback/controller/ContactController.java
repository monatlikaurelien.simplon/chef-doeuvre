package co.simplon.promo18.blogvoyageback.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.blogvoyageback.entities.Contact;
import co.simplon.promo18.blogvoyageback.repository.ContactRepository;
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/contact")
@RestController
public class ContactController {
  @Autowired
  private ContactRepository ctsRepo;
  @GetMapping
  public List<Contact> getAll() {
      return ctsRepo.findAll();
  }

  @PostMapping
  public Contact add(@RequestBody Contact contact) {

      ctsRepo.save(contact);
      return contact;
  }


}
