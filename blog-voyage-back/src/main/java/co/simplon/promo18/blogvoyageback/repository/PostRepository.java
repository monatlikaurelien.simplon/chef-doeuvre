package co.simplon.promo18.blogvoyageback.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.blogvoyageback.entities.Destination;
import co.simplon.promo18.blogvoyageback.entities.Image;
import co.simplon.promo18.blogvoyageback.entities.Post;

@Repository
public class PostRepository {


  @Autowired
  private DataSource dataSource;
  @Autowired
  private ImageRepository imgRepo;

  public List<Post> findAll() {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToPost(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public List<Post> findAllWithDestinationAndImg() {
    List<Post> list = new ArrayList<>();

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "select * from post inner join destination on post.destination_id = destination.id GROUP BY post.id");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post p = new Post(rs.getInt("post.id"), rs.getString("title"),
            rs.getDate("date").toLocalDate(), rs.getString("content"));
        Destination d = new Destination(rs.getInt("destination.id"), rs.getString("label"));
        p.setDestination(d);


        int idPost = rs.getInt("post.id");
        List<Image> listImg = imgRepo.findAllByPost(idPost);
        p.setImages(listImg);
        list.add(p);
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }



  public List<Post> findAllByDestination(int id) {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "select * from post inner join destination on post.destination_id = destination.id WHERE destination_id = ?  GROUP BY post.id");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post p = new Post(rs.getInt("post.id"), rs.getString("title"),
            rs.getDate("date").toLocalDate(), rs.getString("content"));
        Destination d = new Destination(rs.getInt("destination.id"), rs.getString("label"));
        p.setDestination(d);


        int idPost = rs.getInt("post.id");
        List<Image> listImg = imgRepo.findAllByPost(idPost);
        p.setImages(listImg);
        list.add(p);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public Post findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();


      if (rs.next()) {
        return sqlToPost(rs);

      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  public void save(Post post) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO post (title, date, content, destination_id) VALUES (?,?,?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, post.getTitle());
      stmt.setDate(2, Date.valueOf(post.getDate()));
      stmt.setString(3, post.getContent());
      if(post.getDestination() != null) {
        stmt.setInt(4, post.getDestination().getId());
      }

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        post.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM post WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Post post) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE post SET title = ?, date = ?, content = ? WHERE id = ?");

      stmt.setString(1, post.getTitle());
      stmt.setDate(2, java.sql.Date.valueOf(post.getDate()));
      stmt.setString(3, post.getContent());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

  }

  private Post sqlToPost(ResultSet rs) throws SQLException {
    return new Post(rs.getInt("id"), rs.getString("title"), rs.getDate("date").toLocalDate(),
        rs.getString("content"));
  }


}
