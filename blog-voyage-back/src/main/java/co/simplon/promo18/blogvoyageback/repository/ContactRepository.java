package co.simplon.promo18.blogvoyageback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.blogvoyageback.entities.Contact;
@Repository
public class ContactRepository {
  @Autowired
  private DataSource dataSource;

  public List<Contact> findAll() {
    List<Contact> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contact");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {


        list.add(sqlToContact(rs));
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public void save(Contact contact) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO contact (name, firstName, email, message) VALUES (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, contact.getName());
      stmt.setString(2, contact.getFirstName());
      stmt.setString(3, contact.getEmail());
      stmt.setString(4, contact.getMessage());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        contact.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM contact WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }
  private Contact sqlToContact(ResultSet rs) throws SQLException {
    return new Contact(rs.getInt("id"), rs.getString("name"), rs.getString("firstName"), rs.getString("email"), rs.getString("message"));
  }
}
