package co.simplon.promo18.blogvoyageback.controller;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.blogvoyageback.entities.Post;
import co.simplon.promo18.blogvoyageback.repository.PostRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/post")
@RestController
public class PostController {
  @Autowired
  private PostRepository pstRepo;

  @GetMapping("/{id}")
  public List<Post> getAllByDestination(@PathVariable int id) {
    return pstRepo.findAllByDestination(id);
  }

  
  @GetMapping
  public List<Post> getAll() {
    return pstRepo.findAllWithDestinationAndImg();
  }

  @PostMapping
  public Post add(@RequestBody Post post) {
    if (post.getDate() == null) {
      post.setDate(LocalDate.now());
    }

    pstRepo.save(post);
    return post;
  }
}
