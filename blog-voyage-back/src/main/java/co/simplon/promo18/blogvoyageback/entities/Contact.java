package co.simplon.promo18.blogvoyageback.entities;

public class Contact {
  private Integer id;
  private String name;
  private String firstName;
  private String email;
  private String message;
  public Contact() {}
  public Contact(String name, String firstName, String email, String message) {
    this.name = name;
    this.firstName = firstName;
    this.email = email;
    this.message = message;
  }
  public Contact(Integer id, String name, String firstName, String email, String message) {
    this.id = id;
    this.name = name;
    this.firstName = firstName;
    this.email = email;
    this.message = message;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }
}
