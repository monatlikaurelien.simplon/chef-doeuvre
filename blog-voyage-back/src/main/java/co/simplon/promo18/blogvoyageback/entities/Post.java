package co.simplon.promo18.blogvoyageback.entities;

import java.time.LocalDate;
import java.util.List;

public class Post {
  
    private Integer id;
    private String title;
    private LocalDate date;
    private String content;
    private Destination destination;
    private List<Image> images;
    public List<Image> getImages() {
      return images;
    }
    public void setImages(List<Image> images) {
      this.images = images;
    }
    public Destination getDestination() {
      return destination;
    }
    public void setDestination(Destination destination) {
      this.destination = destination;
    }
    public Post() {}
    public Post(String title, LocalDate date, String content) {
      this.title = title;
      this.date = date;
      this.content = content;
    }
    public Post(Integer id, String title, LocalDate date, String content) {
      this.id = id;
      this.title = title;
      this.date = date;
      this.content = content;
    }
    public Integer getId() {
      return id;
    }
    public void setId(Integer id) {
      this.id = id;
    }
    public String getTitle() {
      return title;
    }
    public void setTitle(String title) {
      this.title = title;
    }
    public LocalDate getDate() {
      return date;
    }
    public void setDate(LocalDate date) {
      this.date = date;
    }
    public String getContent() {
      return content;
    }
    public void setContent(String content) {
      this.content = content;
    }
}
