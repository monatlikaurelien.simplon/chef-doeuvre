import { Component, OnInit } from '@angular/core';
import { DestinationService } from '../destination.service';
import { Destination } from '../entities';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  dst?:Destination[];

  constructor(private ds:DestinationService) { }

  ngOnInit(): void {
    this.ds.getAll().subscribe(Data => this.dst = Data);
  }

}
