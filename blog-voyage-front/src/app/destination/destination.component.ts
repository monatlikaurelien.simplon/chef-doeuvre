import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { DestinationService } from '../destination.service';
import { Destination, Post } from '../entities';
import { PostService } from '../post.service';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss']
})
export class DestinationComponent implements OnInit {

  post:Post[] = [];
  destination?:Destination;
  public id! : number;

  constructor(public route : ActivatedRoute, private artservice: PostService, private destService:DestinationService) { }

  ngOnInit(): void {
    this.route.params.pipe(switchMap( params => {
      this.id = params['id'];
      return this.artservice.getById(this.id)
      
    })).subscribe(data => this.post = data)
    this.route.params.pipe(switchMap( params => {
      this.id = params['id'];
      return this.destService.getById(this.id)
      
    })).subscribe(data => this.destination = data)
  }

}
