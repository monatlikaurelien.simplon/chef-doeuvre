import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Contact, Post } from '../entities';
import { PostService } from '../post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  post:Post[] = []
  dstId?:Post;

  constructor(private ps:PostService) { }

  ngOnInit(): void {
    this.ps.getAll().subscribe(data => {this.post = data,
    console.log(this.post);
    });
  }
  



  // fetchOne() {
  //   this.ps.getById(1).subscribe(data => {
  //     this.dstId = data
  //     console.log(this.post)
  //   });
  // }

}
