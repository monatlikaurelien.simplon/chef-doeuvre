import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Image } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http:HttpClient) { }
  getAllByImg() {
    return this.http.get<Image[]>('http://localhost:8080/api/img');
  }
}