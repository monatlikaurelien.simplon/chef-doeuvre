import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './entities';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }
  getAll() {
    return this.http.get<Post[]>('http://localhost:8080/api/post');
  }
  getById(id:number) {
    return this.http.get<Post[]>('http://localhost:8080/api/post/'+id);
  }
  add(post:Post) {
    return this.http.post<Post>('http://localhost:8080/api/post',post);
  }
}
