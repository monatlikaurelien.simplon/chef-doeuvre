import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Contact } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http:HttpClient) { }

  add(contact:Contact){
    return this.http.post<Contact>(environment.apiUrl+'/api/contact', contact);
  }

  getAll() {
  return this.http.get<Contact[]>(environment.apiUrl+'/api/contact');
}
}







