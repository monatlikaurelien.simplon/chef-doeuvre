import { Component, OnInit } from '@angular/core';
import { DestinationService } from '../destination.service';
import { Destination, Post } from '../entities';
import { PostService } from '../post.service';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.scss']
})
export class AddpostComponent implements OnInit {
  post:Post = {
    content:'',
    images:[],
    title:''
  }
  destinations:Destination[] = [];
  constructor(private ps: PostService, private ds:DestinationService) { }

  ngOnInit(): void {
    this.ds.getAll().subscribe(data => this.destinations = data);
  }

  addPost() {
    this.ps.add(this.post).subscribe();
  }

}
