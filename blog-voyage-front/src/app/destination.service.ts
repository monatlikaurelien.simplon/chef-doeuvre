import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Destination } from './entities';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Destination[]>(environment.apiUrl+'/api/destination');
  }
  getById(id:number) {
    return this.http.get<Destination>(environment.apiUrl+'/api/destination/'+id);
  }
}
