import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../contact.service';
import { Contact } from '../entities';

@Component({
  selector: 'app-contactform',
  templateUrl: './contactform.component.html',
  styleUrls: ['./contactform.component.scss']
})
export class ContactformComponent implements OnInit {

  contact:Contact = {
    name: '',
    firstName: '',
    email: '',
    message:''
  };


  constructor(private cs:ContactService, private router:Router) { }

  ngOnInit(): void {
  }

  addForm() {
    this.cs.add(this.contact).subscribe(() => {
      this.router.navigate(['/contact']);
    });
  }

}
