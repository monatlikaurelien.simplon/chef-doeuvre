import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddpostComponent } from './addpost/addpost.component';
import { BlogComponent } from './blog/blog.component';
import { ContactformComponent } from './contactform/contactform.component';
import { DestinationComponent } from './destination/destination.component';
import { HomeComponent } from './home/home.component';
import { PhotoComponent } from './photo/photo.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'contact', component:ContactformComponent},
  {path:'destination/:id', component:DestinationComponent},
  {path: 'blog' , component:BlogComponent},
  {path: 'addpost' , component:AddpostComponent},
  {path:'photo', component:PhotoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
