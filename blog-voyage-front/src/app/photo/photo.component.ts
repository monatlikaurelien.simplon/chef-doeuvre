import { Component, OnInit } from '@angular/core';
import { Image } from '../entities';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {

  img:Image[] = []

  constructor(private imgservice:ImageService) { }

  ngOnInit(): void {
    this.imgservice.getAllByImg().subscribe(data => {this.img = data,
      console.log(this.img);
      });
  }

}
