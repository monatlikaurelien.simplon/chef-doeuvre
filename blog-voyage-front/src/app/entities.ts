export interface Destination {
  id?:number;
  label:string;
}


export interface Post {
  id?:number;
  title:string;
  date?:Date;
  content:string;
  destination?:Destination;
  images:Image[];
}

export interface Image {
  id?:number;
  label:string;
  upload:string;
  post_id:number;
}

export interface Contact {
  id?:number;
  name:string;
  firstName:string;
  email:string;
  message:string;
}

